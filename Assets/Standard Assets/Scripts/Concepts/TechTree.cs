using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SurviveGame
{
	[Serializable]
	public class TechTree : TreeNode<TechTreeNode>
	{
		public TechTreeNode[] topNodes = new TechTreeNode[0];

		public void Init ()
		{
			List<TreeNode<TechTreeNode>> remainingChildren = new List<TreeNode<TechTreeNode>>();
			for (int i = 0; i < topNodes.Length; i ++)
			{
				TechTreeNode topNode = topNodes[i];
				remainingChildren.Add(new TreeNode<TechTreeNode>(topNode));
			}
			while (remainingChildren.Count > 0)
			{
				TreeNode<TechTreeNode> currentNode = remainingChildren[0];
				for (int i = 0; i < currentNode.value.nextNodes.Length; i ++)
				{
					TechTreeNode nextNode = currentNode.value.nextNodes[i];
					currentNode.AddChild (nextNode);
					remainingChildren.Add(new TreeNode<TechTreeNode>(nextNode));
				}
				remainingChildren.RemoveAt(0);
			}
			AddChildren (topNodes);
		}
	}
}