using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SurviveGame
{
	public class GameCamera : CameraScript
	{
		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			Player.instance = Player.Instance;
			trs.SetParent(null);
			base.Awake ();
		}

		public override void HandlePosition ()
		{
            trs.position = Player.instance.trs.position.SetZ(trs.position.z);
            base.HandlePosition ();
		}
	}
}
