﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColliderPresenceDetector : MonoBehaviour
{
	[HideInInspector]
	public List<Collider> collidersInside = new List<Collider>();
	
	void OnTriggerEnter (Collider other)
	{
		if (enabled)
			collidersInside.Add(other);
	}
	
	void OnTriggerExit (Collider other)
	{
		if (enabled)
			collidersInside.Remove(other);
	}
	
	void OnDisable ()
	{
		collidersInside.Clear();
	}
}
