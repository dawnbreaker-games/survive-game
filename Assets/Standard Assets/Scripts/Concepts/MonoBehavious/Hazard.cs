﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SurviveGame
{
	public class Hazard : Spawnable
	{
		public int damage;
		public bool destroyOnContact;
		[HideInInspector]
		public bool dead;
		
		public virtual void OnCollisionEnter2D (Collision2D coll)
		{
			if (dead)
				return;
			IDestructable destructable = coll.gameObject.GetComponent<IDestructable>();
			if (destructable != null)
				ApplyDamage (destructable, damage);
			if (destroyOnContact)
			{
				dead = true;
				if (prefabIndex == -1)
					Destroy(gameObject);
				else
					// ObjectPool.Instance.Despawn (prefabIndex, gameObject, trs);
					Destroy(gameObject);
			}
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			if (dead)
				return;
			IDestructable destructable = other.GetComponent<IDestructable>();
			if (destructable != null)
				ApplyDamage (destructable, damage);
			if (destroyOnContact)
			{
				dead = true;
				if (prefabIndex == -1)
					Destroy(gameObject);
				else
					// ObjectPool.Instance.Despawn (prefabIndex, gameObject, trs);
					Destroy(gameObject);
			}
		}
		
		public virtual void ApplyDamage (IDestructable destructable, int amount)
		{
			destructable.TakeDamage (amount);
		}
	}
}