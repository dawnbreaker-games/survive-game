using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
using System.Collections.Generic;

namespace SurviveGame
{
	public class WorldPiece : MonoBehaviour
	{
		public Transform trs;
		public Vector2Int location;
		public Tilemap groundTilemap;
		public RectInt cellBoundsRect;
		public Rect worldBoundsRect;
	}
}
