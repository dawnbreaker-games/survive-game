﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SingletonScriptableObject<T> : ScriptableObject where T : ScriptableObject
{
	public static T instance;
	public static T Instance
	{
		get
		{
			if (instance == null)
				instance = FindObjectOfType<T>();
			return instance;
		}
	}
	
	public virtual void Awake ()
	{
		instance = this as T;
	}
}
