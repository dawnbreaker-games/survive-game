﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoseableScenerio : SingletonMonoBehaviour<LoseableScenerio>
{
	public static List<LoseableScenerio> activeScenarios = new List<LoseableScenerio>();
	
	void OnEnable ()
	{
		activeScenarios.Add(this);
	}
	
	void OnDisable ()
	{
		activeScenarios.Remove(this);
	}
	
	public void Lose ()
	{
		// _SceneManager.instance.LoadLevelWithTransition("Game Over");
		_SceneManager.instance.RestartSceneWithoutTransition ();
	}
}
