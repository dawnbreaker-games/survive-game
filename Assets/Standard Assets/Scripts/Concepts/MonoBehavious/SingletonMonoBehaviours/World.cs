using System;
using Extensions;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SurviveGame
{
	public class World : SingletonUpdateWhileEnabled<World>
	{
		public WorldPiece piecePrefab;
		public Dictionary<Vector2Int, WorldPiece> piecesDict = new Dictionary<Vector2Int, WorldPiece>();
		public Grid grid;
		public Transform piecesParent;
		public Vector2Int loadPiecesRange;
		public Vector2Int piecesCellSize;
		public List<WorldPiece> piecesNearPlayer = new List<WorldPiece>();
		public List<WorldPiece> activePieces = new List<WorldPiece>();
		public TileEntry[] tileEntries = new TileEntry[0];
		public float enemySpatialHashGridCellSize;
		public static ResizableSpatialHashGrid2D<Enemy> enemySpatialHashGrid;
		ResizableSpatialHashGrid2D<Enemy>.Agent[] activeEnemySpatialHashGridAgents = new ResizableSpatialHashGrid2D<Enemy>.Agent[0];
		HashSet<WorldPiece> surroundingPieces = new HashSet<WorldPiece>();
		Rect loadPieceRangeRect;

		public override void Awake ()
		{
			base.Awake ();
			enemySpatialHashGrid = new ResizableSpatialHashGrid2D<Enemy>(enemySpatialHashGridCellSize);
			WorldPiece worldPiece = MakeNewPiece(Vector2Int.zero);
			activePieces.Add(worldPiece);
			piecesNearPlayer.Add(worldPiece);
			WorldPiece piece;
			piece = piecesDict[Vector2Int.zero];
			surroundingPieces = GetSurroundingPieces(piece);
			foreach (WorldPiece surroundingPiece in surroundingPieces)
			{
				loadPieceRangeRect = surroundingPiece.worldBoundsRect.Expand(loadPiecesRange * 2);
				if (GameCamera.Instance.viewRect.IsIntersecting(loadPieceRangeRect))
				{
					piecesNearPlayer.Add(surroundingPiece);
					activePieces.Add(surroundingPiece);
					surroundingPiece.gameObject.SetActive(true);
				}
				else
					surroundingPiece.gameObject.SetActive(false);
			}
			piecesNearPlayer.AddRange(GetSurroundingPieces(activePieces.ToArray()));
		}

		public override void DoUpdate ()
		{
			for (int i = 0; i < piecesNearPlayer.Count; i ++)
			{
				WorldPiece pieceNearPlayer = piecesNearPlayer[i];
				loadPieceRangeRect = pieceNearPlayer.worldBoundsRect.Expand(loadPiecesRange * 2);
				if (GameCamera.instance.viewRect.IsIntersecting(loadPieceRangeRect))
				{
					if (!pieceNearPlayer.gameObject.activeSelf)
					{
						pieceNearPlayer.gameObject.SetActive(true);
						if (!activePieces.Contains(pieceNearPlayer))
						{
							activePieces.Add(pieceNearPlayer);
						}
						surroundingPieces = GetSurroundingPieces(pieceNearPlayer);
						foreach (WorldPiece surroundingPiece in surroundingPieces)
						{
							if (!piecesNearPlayer.Contains(surroundingPiece))
								piecesNearPlayer.Add(surroundingPiece);
						}
					}
				}
				else if (pieceNearPlayer.gameObject.activeSelf)
				{
					pieceNearPlayer.gameObject.SetActive(false);
					activePieces.Remove(pieceNearPlayer);
				}
				else if (!GetSurroundingPieces(activePieces.ToArray()).Contains(pieceNearPlayer))
				{
					piecesNearPlayer.RemoveAt(i);
					i --;
				}
			}
			ResizableSpatialHashGrid2D<Enemy>.Agent[] closebyEnemySpatialHashGridAgents = enemySpatialHashGrid.GetClosebyAgents(Player.instance.trs.position);
			for (int i = 0; i < activeEnemySpatialHashGridAgents.Length; i ++)
			{
				ResizableSpatialHashGrid2D<Enemy>.Agent activeEnemySpatialHashGridAgent = activeEnemySpatialHashGridAgents[i];
				activeEnemySpatialHashGridAgent.value.gameObject.SetActive(false);
			}
			for (int i = 0; i < closebyEnemySpatialHashGridAgents.Length; i ++)
			{
				ResizableSpatialHashGrid2D<Enemy>.Agent closebyEnemySpatialHashGridAgent = closebyEnemySpatialHashGridAgents[i];
				closebyEnemySpatialHashGridAgent.value.gameObject.SetActive(true);
			}
			activeEnemySpatialHashGridAgents = new ResizableSpatialHashGrid2D<Enemy>.Agent[closebyEnemySpatialHashGridAgents.Length];
			closebyEnemySpatialHashGridAgents.CopyTo(activeEnemySpatialHashGridAgents, 0);
		}

		HashSet<WorldPiece> GetSurroundingPieces (params WorldPiece[] innerPieces)
		{
			HashSet<WorldPiece> output = new HashSet<WorldPiece>();
			for (int i = 0; i < innerPieces.Length; i ++)
			{
				WorldPiece innerPiece = innerPieces[i];
				output.Add(GetPiece(innerPiece.location + Vector2Int.right));
				output.Add(GetPiece(innerPiece.location + Vector2Int.up));
				output.Add(GetPiece(innerPiece.location + Vector2Int.left));
				output.Add(GetPiece(innerPiece.location + Vector2Int.down));
				output.Add(GetPiece(innerPiece.location + Vector2Int.right + Vector2Int.up));
				output.Add(GetPiece(innerPiece.location + Vector2Int.right + Vector2Int.down));
				output.Add(GetPiece(innerPiece.location + Vector2Int.left + Vector2Int.up));
				output.Add(GetPiece(innerPiece.location + Vector2Int.left + Vector2Int.down));
			}
			for (int i = 0; i < innerPieces.Length; i ++)
			{
				WorldPiece innerPiece = innerPieces[i];
				output.Remove(innerPiece);
			}
			return output;
		}

		WorldPiece GetPiece (Vector2Int location)
		{
			WorldPiece piece;
			if (piecesDict.TryGetValue(location, out piece))
				return piece;
			else
				return MakeNewPiece(location);
		}

		WorldPiece MakeNewPiece (Vector2Int location)
		{
			WorldPiece output = Instantiate(piecePrefab, piecesParent);
			output.location = location;
			output.cellBoundsRect = new RectInt(location * piecesCellSize, piecesCellSize);
			Vector3 pieceWorldBoundsMin = grid.CellToWorld((location * piecesCellSize).ToVec3Int());
			output.worldBoundsRect = new Rect(pieceWorldBoundsMin, grid.CellToWorld((location * piecesCellSize + piecesCellSize + Vector2Int.one).ToVec3Int()) - pieceWorldBoundsMin);
			Vector2Int minCellPosition = output.cellBoundsRect.min;
			Vector2Int maxCellPosition = output.cellBoundsRect.max;
			List<TileChangeData> tileChangeDatas = new List<TileChangeData>();
			for (int x = minCellPosition.x; x < maxCellPosition.x; x ++)
			{
				for (int y = minCellPosition.y; y < maxCellPosition.y; y ++)
				{
					Vector3Int cellPosition = new Vector3Int(x, y);
					TileEntry tileEntry = tileEntries[Random.Range(0, tileEntries.Length)];
					Color color;
					if (tileEntry.useGradient)
						color = tileEntry.gradient.Evaluate(Random.value);
					else
						color = tileEntry.colorPalette.Get(Random.value);
					TileChangeData tileChangeData = new TileChangeData();
					tileChangeData.position = cellPosition;
					tileChangeData.tile = tileEntry.tile;
					tileChangeData.color = color;
					tileChangeData.transform = Matrix4x4.identity;
					tileChangeDatas.Add(tileChangeData);
				}
			}
			output.groundTilemap.SetTiles(tileChangeDatas.ToArray(), true);
			piecesDict.Add(location, output);
			return output;
		}

		[Serializable]
		public struct TileEntry
		{
			public TileBase tile;
			public bool useGradient;
			public ColorPalette colorPalette;
			public Gradient gradient;
		}
	}
}
