﻿using SurviveGame;
using System.Collections;
using System.Collections.Generic;

public class GameOverScreen : SingletonMonoBehaviour<GameOverScreen>
{
	public void GameOver ()
	{
		_SceneManager.instance.LoadSceneWithTransition (_SceneManager.instance.mostRecentSceneName);
	}
}
