using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SurviveGame
{
	[Serializable]
	public class SpawnZone2D : Shape2D
	{
		public Transform[] cornerTransforms = new Transform[0];
		public Collider2D collider;
		public Type type;

		public SpawnZone2D (Transform[] cornerTransforms)
		{
			type = Type.CornerTransforms;
			SetCorners (cornerTransforms);
			SetEdges ();
		}

		public SpawnZone2D (Collider2D collider)
		{
			type = Type.ColliderRect;
			SetCorners (collider);
			SetEdges ();
		}

		void SetCorners (Transform[] cornerTransforms)
		{
			corners = new Vector2[cornerTransforms.Length];
			for (int i = 0; i < cornerTransforms.Length; i ++)
			{
				Transform cornerTrs = cornerTransforms[i];
				corners[i] = cornerTrs.position;
			}
		}

		void SetCorners (Collider2D collider)
		{
			Bounds colliderBounds = collider.bounds;
			corners = new Vector2[4];
			corners[0] = colliderBounds.min;
			corners[1] = new Vector2(colliderBounds.min.x, colliderBounds.max.y);
			corners[2] = colliderBounds.max;
			corners[3] = new Vector2(colliderBounds.max.x, colliderBounds.min.y);
		}

		public void SetCornersAndEdges (Type type)
		{
			if (type == Type.CornerTransforms)
				SetCorners (cornerTransforms);
			else
				SetCorners (collider);
			SetEdges ();
		}

		public enum Type
		{
			CornerTransforms,
			ColliderRect
		}
	}
}