using System;
using UnityEngine;
using System.Collections;

namespace SurviveGame
{
	[Serializable]
	public class AttackEntry
	{
		public BulletPattern bulletPattern;
		public Bullet bulletPrefab;
		public Transform spawner;
		public float attackInterval;
		public float timeRemainingTillAttack;

		public void Update ()
		{
			timeRemainingTillAttack -= Time.deltaTime;
			if (timeRemainingTillAttack <= 0)
			{
				Attack ();
				timeRemainingTillAttack += attackInterval;
			}
		}
		
		void Attack ()
		{
			bulletPattern.Shoot (spawner, bulletPrefab);
		}
	}
}