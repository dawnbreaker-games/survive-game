using System;
using Extensions;
using UnityEngine;

[Serializable]
public class ColorPalette
{
	public ColorRange colorRange;
	public ColorOffset[] colorOffsets = new ColorOffset[0];

	public Color Get (float normalizedValue)
	{
		Color output = colorRange.Get(normalizedValue);
		Color[] colors = new Color[colorOffsets.Length];
		for (int i = 0; i < colorOffsets.Length; i ++)
		{
			ColorOffset colorOffset = colorOffsets[i];
			colors[i] = colorOffset.Apply(output);
		}
		return ColorExtensions.GetAverage(colors);
	}

	public Color GetWithTransparency (float normalizedValue)
	{
		Color output = colorRange.Get(normalizedValue);
		Color[] colors = new Color[colorOffsets.Length];
		for (int i = 0; i < colorOffsets.Length; i ++)
		{
			ColorOffset colorOffset = colorOffsets[i];
			colors[i] = colorOffset.ApplyWithTransparency(output);
		}
		return ColorExtensions.GetAverage(colors);
	}
}