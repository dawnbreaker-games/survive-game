﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SurviveGame
{
	[CreateAssetMenu]
	public class AimInDirection : BulletPattern
	{
		public Vector3 shootDirection;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			spawner.up = GetShootDirection(spawner);
			return base.Shoot(spawner, bulletPrefab, positionOffset);
		}

		public override Vector3 GetShootDirection (Transform spawner)
		{
			return shootDirection;
		}
	}
}