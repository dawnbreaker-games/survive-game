﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SurviveGame
{
	[CreateAssetMenu]
	public class FollowClosestEnemy : AimWhereFacing
	{
		public LayerMask whatIsEnemy;
		public float rotateRate;
		public float followRange;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab, positionOffset);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.StartCoroutine(FollowRoutine (bullet));
			}
			return output;
		}
		
		public override Bullet[] Shoot (Vector2 spawnPos, Vector2 direction, Bullet bulletPrefab, float positionOffset = 0)
		{
			Bullet[] output = base.Shoot(spawnPos, direction, bulletPrefab, positionOffset);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.StartCoroutine(FollowRoutine (bullet));
			}
			return output;
		}

		public virtual IEnumerator FollowRoutine (Bullet bullet)
		{
			while (true)
			{
				Collider[] hits = Physics.OverlapSphere(bullet.trs.position, followRange, whatIsEnemy);
				if (hits.Length > 0)
				{
					Vector3 hitPosition = hits[0].bounds.center;
					Vector3 closestHitPosition = hitPosition;
					float hitDistanceSqr = 0;
					float closestHitDistanceSqr = Mathf.Infinity;
					for (int i = 1; i < hits.Length; i ++)
					{
						hitPosition = hits[i].bounds.center;
						hitDistanceSqr = (hitPosition - bullet.trs.position).sqrMagnitude;
						if (hitDistanceSqr < closestHitDistanceSqr)
						{
							closestHitDistanceSqr = hitDistanceSqr;
							closestHitPosition = hitPosition;
						}
					}
					Vector3 velocity = Vector3.RotateTowards(bullet.rigid.velocity, closestHitPosition - bullet.trs.position, rotateRate * Mathf.Deg2Rad * Time.deltaTime, 0);
					bullet.rigid.velocity = velocity;
				}
				yield return new WaitForEndOfFrame();
			}
		}
	}
}