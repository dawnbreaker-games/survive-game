using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public struct ResizableSpatialHashGrid2D<T>
{
	public float cellSize;
	public Dictionary<Vector2Int, List<Agent>> agentsDict;

	public ResizableSpatialHashGrid2D (float cellSize)
	{
		this.cellSize = cellSize;
		agentsDict = new Dictionary<Vector2Int, List<Agent>>();
	}

	public Vector2Int Evaluate (Vector2 position)
	{
		return (position / cellSize).ToVec2Int();
	}

	public Agent[] GetClosebyAgents (Vector2 position)
	{
		return GetClosebyAgents(Evaluate(position));
	}

	public Agent[] GetClosebyAgents (Vector2Int cellPosition)
	{
		List<Agent> output = new List<Agent>(GetAgents(cellPosition));
		output.AddRange(GetAgents(new Vector2Int(cellPosition.x - 1, cellPosition.y - 1)));
		output.AddRange(GetAgents(new Vector2Int(cellPosition.x - 1, cellPosition.y)));
		output.AddRange(GetAgents(new Vector2Int(cellPosition.x, cellPosition.y - 1)));
		output.AddRange(GetAgents(new Vector2Int(cellPosition.x + 1, cellPosition.y + 1)));
		output.AddRange(GetAgents(new Vector2Int(cellPosition.x + 1, cellPosition.y)));
		output.AddRange(GetAgents(new Vector2Int(cellPosition.x, cellPosition.y + 1)));
		output.AddRange(GetAgents(new Vector2Int(cellPosition.x - 1, cellPosition.y + 1)));
		output.AddRange(GetAgents(new Vector2Int(cellPosition.x + 1, cellPosition.y - 1)));
		return output.ToArray();
	}

	public Agent[] GetAgents (Vector2Int cellPosition)
	{
		List<Agent> output;
		if (agentsDict.TryGetValue(cellPosition, out output))
			return output.ToArray();
		else
			return new Agent[0];
	}

	[Serializable]
	public struct Agent
	{
		public Vector2 position;
		public Vector2Int cellPosition;
		public T value;
		public ResizableSpatialHashGrid2D<T> spatialHashGrid;

		public Agent (Vector2 position, T value, ResizableSpatialHashGrid2D<T> spatialHashGrid)
		{
			this.position = position;
			cellPosition = spatialHashGrid.Evaluate(position);
			this.value = value;
			this.spatialHashGrid = spatialHashGrid;
			if (spatialHashGrid.agentsDict.ContainsKey(cellPosition))
				spatialHashGrid.agentsDict[cellPosition].Add(this);
			else
				spatialHashGrid.agentsDict.Add(cellPosition, new List<Agent>() { this });
		}

		public void Remove ()
		{
			if (spatialHashGrid.agentsDict[cellPosition].Count > 1)
				spatialHashGrid.agentsDict[cellPosition].Remove(this);
			else
				spatialHashGrid.agentsDict.Remove(cellPosition);
		}

		public void Update (Vector2 position)
		{
			Remove ();
			this.position = position;
			cellPosition = spatialHashGrid.Evaluate(position);
			if (spatialHashGrid.agentsDict.ContainsKey(cellPosition))
				spatialHashGrid.agentsDict[cellPosition].Add(this);
			else
				spatialHashGrid.agentsDict.Add(cellPosition, new List<Agent>() { this });
		}
	}
}
