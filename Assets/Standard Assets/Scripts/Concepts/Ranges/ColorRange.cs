using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class ColorRange : Range<Color>
{
	public static ColorRange NULL = new ColorRange(ColorExtensions.NULL, ColorExtensions.NULL);

	public ColorRange (Color min, Color max) : base (min, max)
	{
	}

	public override Color Get (float normalizedValue)
	{
		return Color.Lerp(min, max, normalizedValue);
	}
}