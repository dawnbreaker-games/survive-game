using UnityEngine;

namespace SurviveGame
{
	public class Enemy : UpdateWhileEnabled, IDestructable
	{
		int hp;
		public int Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return 0;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		public Rigidbody2D rigid;
		public float moveSpeed;
		public ResizableSpatialHashGrid2D<Enemy>.Agent spatialHashGridAgent;

		public virtual void Awake ()
		{
			hp = maxHp;
			spatialHashGridAgent = new ResizableSpatialHashGrid2D<Enemy>.Agent(trs.position, this, World.enemySpatialHashGrid);
		}

		public override void DoUpdate ()
		{
			HandleMovement ();
		}

		void HandleMovement ()
		{
			Move (Player.instance.trs.position - trs.position);
		}

		void Move (Vector2 move)
		{
			rigid.velocity = move.normalized * moveSpeed;
			spatialHashGridAgent.Update (trs.position);
		}
		
		public void TakeDamage (int amount)
		{
			hp -= amount;
			if (hp <= 0)
				Death ();
		}
		
		public void Death ()
		{
			Destroy(gameObject);
		}
	}
}
