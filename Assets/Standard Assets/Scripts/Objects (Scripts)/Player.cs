using System;
using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace SurviveGame
{
	public class Player : SingletonUpdateWhileEnabled<Player>, IDestructable
	{
		int hp;
		public int Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return 0;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		public Rigidbody2D rigid;
		public Collider2D collider;
		public SpriteRenderer spriteRenderer;

		public override void Awake ()
		{
			base.Awake ();
			hp = maxHp;
		}

		public override void DoUpdate ()
		{
			// if (InputManager.MoveInput != Vector2.zero)
			// 	trs.up = InputManager.MoveInput;
			if (InputManager.MoveInput != Vector2.zero)
				spriteRenderer.flipX = InputManager.MoveInput.x < 0;
			HandleMovement ();
		}

		void HandleMovement ()
		{
			Move (InputManager.MoveInput);
		}

		void Move (Vector2 move)
		{
			move = move.Multiply(GameCamera.pixelSize);
			List<RaycastHit2D> hits = new List<RaycastHit2D>();
			if (rigid.Cast(move, hits, move.magnitude) == 0)
				trs.position += (Vector3) move;
			else
				trs.position += (Vector3) move.normalized * hits[0].distance;
			World.instance.DoUpdate ();
		}
		
		public void TakeDamage (int amount)
		{
			hp -= amount;
			if (hp <= 0)
				Death ();
		}
		
		public void Death ()
		{
			_SceneManager.instance.RestartSceneWithoutTransition ();
		}
	}
}
