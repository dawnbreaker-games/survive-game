using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SurviveGame
{
	public class ShootingEnemy : Enemy
	{
		public AttackEntry[] attackEntries;
		
		public override void DoUpdate ()
		{
			base.DoUpdate ();
			HandleAttacking ();
		}
		
		void HandleAttacking ()
		{
			for (int i = 0; i < attackEntries.Length; i ++)
			{
				AttackEntry attackEntry = attackEntries[i];
                attackEntry.Update ();
			}
		}
	}
}