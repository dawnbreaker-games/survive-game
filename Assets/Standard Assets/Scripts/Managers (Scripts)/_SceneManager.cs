﻿using SurviveGame;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class _SceneManager : SingletonMonoBehaviour<_SceneManager>, ISavableAndLoadable
{
	public static bool isLoading;
	public float transitionRate;
	[SaveAndLoadValue]
	public string mostRecentSceneName;
	public static Scene CurrentScene
	{
		get
		{
			return SceneManager.GetActiveScene();
		}
	}
	
	public void LoadSceneWithTransition (string sceneName)
	{
		if (instance != this)
		{
			instance.LoadSceneWithTransition (sceneName);
			return;
		}
		isLoading = true;
		Time.timeScale = 1;
		StartCoroutine(SceneTransition (sceneName));
	}
	
	public void LoadSceneWithoutTransition (string sceneName)
	{
		isLoading = true;	
		Time.timeScale = 1;
		SceneManager.LoadScene(sceneName);
	}
	
	public void LoadSceneWithTransition (int sceneId)
	{
		if (instance != this)
		{
			instance.LoadSceneWithTransition (sceneId);
			return;
		}
		isLoading = true;
		Time.timeScale = 1;
		StartCoroutine(SceneTransition (sceneId));
	}
	
	public void LoadSceneWithoutTransition (int sceneId)
	{
		isLoading = true;
		Time.timeScale = 1;
		SceneManager.LoadScene(sceneId);
	}
	
	public void LoadSceneAdditiveWithTransition (string sceneName)
	{
		if (instance != this)
		{
			instance.LoadSceneAdditiveWithTransition (sceneName);
			return;
		}
		isLoading = true;
		Time.timeScale = 1;
		StartCoroutine(SceneTransition (sceneName, LoadSceneMode.Additive));
	}
	
	public void LoadSceneAdditiveWithoutTransition (string sceneName)
	{
		isLoading = true;
		Time.timeScale = 1;
		SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
	}
	
	public void RestartSceneWithTransition ()
	{
		isLoading = true;
		LoadSceneWithTransition (SceneManager.GetActiveScene().name);
	}
	
	public void RestartSceneWithoutTransition ()
	{
		isLoading = true;
		LoadSceneWithoutTransition (SceneManager.GetActiveScene().name);
	}
	
	public void NextSceneWithTransition ()
	{
		isLoading = true;
		LoadSceneWithTransition (SceneManager.GetActiveScene().buildIndex + 1);
	}
	
	public void NextSceneWithoutTransition ()
	{
		isLoading = true;
		LoadSceneWithoutTransition (SceneManager.GetActiveScene().buildIndex + 1);
	}
	
	public virtual void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
	{
		Camera.main.rect = new Rect(.5f, .5f, 0, 0);
		StartCoroutine(SceneTransition (null));
		SceneManager.sceneLoaded -= OnSceneLoaded;
		isLoading = false;
	}
	
	public virtual IEnumerator SceneTransition (string sceneName = null, LoadSceneMode loadMode = LoadSceneMode.Single)
	{
		bool transitioningIn = string.IsNullOrEmpty(sceneName);
		float transitionRateMultiplier = 1;
		if (transitioningIn)
			transitionRateMultiplier *= -1;
		while ((Camera.main.rect.size.x > 0 && !transitioningIn) || (Camera.main.rect.size.x < 1 && transitioningIn))
		{
			Rect cameraRect = Camera.main.rect;
			cameraRect.size -= Vector2.one * transitionRate * transitionRateMultiplier * Time.unscaledDeltaTime;
			cameraRect.center += Vector2.one * transitionRate * transitionRateMultiplier * Time.unscaledDeltaTime / 2;
			Camera.main.rect = cameraRect;
			yield return new WaitForEndOfFrame();
		}
		if (transitioningIn)
			Camera.main.rect = new Rect(0, 0, 1, 1);
		else
		{
			Camera.main.rect = new Rect(.5f, .5f, 0, 0);
			SceneManager.sceneLoaded += OnSceneLoaded;
			if (!string.IsNullOrEmpty(sceneName))
				SceneManager.LoadScene(sceneName, loadMode);
		}
	}

	public virtual IEnumerator SceneTransition (int sceneId = -1, LoadSceneMode loadMode = LoadSceneMode.Single)
	{
		yield return StartCoroutine(SceneTransition (SceneManager.GetSceneByBuildIndex(sceneId).name, loadMode));
	}
}
