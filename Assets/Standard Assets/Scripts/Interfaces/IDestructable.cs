using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SurviveGame
{
	public interface IDestructable
	{
		int Hp { get; set; }
		int MaxHp { get; set; }
		
		void TakeDamage (int amount);
		void Death ();
	}
}